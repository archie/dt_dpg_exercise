# DT_DPG_exercise



## Getting started
### 1. Create the working area


Login in your  DESY account
    

Now source CMSSW & setup your working area


```bash
source /cvmfs/cms.cern.ch/cmsset_default.sh
mkdir DTDPG_DAS
cd DTDPG_DAS
cmsrel CMSSW_13_2_2
cd CMSSW_13_2_2/src
cmsenv
```
### 2. Clone this repository in your working area

Clone this repo in your working area in the CMSSWxxx/src area 

```bash
git clone https://gitlab.cern.ch/cms-podas23/dpg/dt-dpg-exercise.git 
```

The main code is in .C file which runs on the ntuples produced for muons DPG studies. The code looks for the reco muons, we apply some quality selections on the muons only coming from the barrel region. Then we look for the standalone muons matched with DT segment to extract the basic informations of these segments on the detector level. For example from which part of the detector they are coming (wheel, station), occupancy 


### 3. Running the code in root 
```bash
cd dt-dpg-exercise
root -l -b dtDPG_segmentAnalysis.C
```

Check the plots saved in the "results"
folder. Lets start with the occupancy plot with respect to muon eta & phi

### 3. Update the code (using any of your fav. editor)

####a. Muon Occupancy

We have already plotted the occupancy of muons which are the tight muons in barrel region with pT > 25 GeV. Tight muons selections contains the cut of "matchedMuonStations >= 2". You have to add a plot of muon occupancy requiring number of matchedmuonstations >= 3 
Run the code
Compare the two plots and try to see what information you can see in the plot you have drawn when requiring the tight selection 

####b. Hit Multiplicity

Add a plot to check the number of hits which are present in the segment & run the code.
 What do you expect how many hits should be there in a segment and what information the plot gives you about the qulaity of the segment

####c. DT segment timing

In the results folder, there is a plots of DT segment time, excatly peaked at zero. Update the code to remove the selection requiring good quality muons (toght muon selections & pt cut). Add another plot of DT segment time and put the y-axis on log scale.
Run the code.
 How the shape of the plot looks now and what information it provides you. 


### 5. More information
If you are stuck or having any difficulty editing the file, there is the another version of file "dtDPG_segmentAnalysisFinal.C" you can check for the help