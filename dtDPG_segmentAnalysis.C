// A _very_ simple MuDPGNtuple TTreeReader-based example

#include <filesystem>
#include <iostream>
#include <map>

#include "TCanvas.h"
#include "TColor.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TROOT.h"
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include "TTreeReaderValue.h"
#include "TLegend.h"
#include "TGraphAsymmErrors.h"
#include "TPaveStats.h"
#include "TStyle.h"

void dtDPG_segmentAnalysis() {
  gROOT->ProcessLine("gErrorIgnoreLevel = 1001;");
 std::string FILE_PATH{"/afs/cern.ch/work/a/archie/public/nano_mu_RAW2DIGI_NANO_ZMu_Run2023D_v1.root"};
 //std::string FILE_PATH{"/nfs/dust/cms/group/cmsdas2023/Muon-DPG/DTDPGexercise/CMSSW_13_2_2/src/nano_mu_RAW2DIGI_NANO_ZMu_Run2023D_v1.root"};
  std::string RESULTS_FOLDER{"results/"};
  const double MAX_ABS_ETA{1.2};
  const double MIN_PT{25.0};

  // Plot booking
  std::map<std::string, TH1 *> histos;
  std::map<std::string, TH2 *> histos2D;

  TH1F *dt_seg_time_log;
  TH1F *dt_seg_nHitsPhi;
  TH2F *mu_eta_phi_tightmuons_3matchedstations;

  histos["mu_pt"] = new TH1F("mu_pt", ";#mu p_{T};entries", 100, 0.0, 100.0);
  histos["mu_n_staMatches"] = new TH1F("mu_n_staMatches", ";#mu # matches;entries", 11, -0.5, 10.5);

  histos["dt_seg_wheel"] = new TH1F("dt_seg_wheel", ";segment wheel;entries", 5, -2.5, 2.5);
  histos["dt_seg_station"] = new TH1F("dt_seg_station", ";segment station;entries", 4, 0.5, 4.5);
  histos["dt_seg_sector"] = new TH1F("dt_seg_sector", ";segment sector;entries", 12, 0.5, 12.5);
  histos["dt_seg_time"] = new TH1F("dt_seg_time", ";segment time;entries", 80, -200, 200);
  histos2D["mu_eta_phi_tightmuons"] =  new TH2F("mu_eta_phi_tightmuons", ";#mu #eta ;#mu #phi, entries", 56, -1.2, 1.2, 180, -TMath::Pi(), TMath::Pi());
  histos2D["dt_seg_WheelvsStation"] =  new TH2F("dt_seg_WheelvsStation", "; sectors ;wheels", 12, -0.5, 11.5, 5, -2.5, 2.5);

  dt_seg_time_log = new TH1F("dt_seg_time_log", ";segment time;entries", 80, -200, 200);
  dt_seg_nHitsPhi =  new TH1F("dt_seg_nHitsPhi", "; # of segment hits (#phi );entries", 12, -0.5, 11.5);
  mu_eta_phi_tightmuons_3matchedstations =  new TH2F("mu_eta_phi_tightmuons_3matchedstations", ";#mu #eta ;#mu #phi, entries", 56, -1.2, 1.2, 180, -TMath::Pi(), TMath::Pi());

  // Open the file containing the tree
  TFile *myFile = TFile::Open(FILE_PATH.c_str());

  // Create a TTreeReader for the tree, for instance by passing the
  // TTree's name and the TDirectory / TFile it is in.
  TTreeReader reader{"Events", myFile};

  // Using TTreeReader, you just just set to read the branches you need

  TTreeReaderValue<Int_t> n_mu{reader, "nmuon"};
  TTreeReaderArray<Float_t> mu_pt{reader, "muon_pt"};
  TTreeReaderArray<Float_t> mu_eta{reader, "muon_eta"};
  TTreeReaderArray<Float_t> mu_phi{reader, "muon_phi"};
  TTreeReaderArray<Bool_t> mu_isTight{reader, "muon_isTight"};
  TTreeReaderArray<Char_t> mu_matchedstations{reader, "muon_trkMu_numberOfMatchedStations"};

  TTreeReaderArray<UInt_t> muon_staMatches_begin{reader, "muon_staMatches_begin"};
  TTreeReaderArray<UInt_t> muon_staMatches_end{reader, "muon_staMatches_end"};
  TTreeReaderArray<UInt_t> mu_staMatches_MuSegIdx{reader, "muon_staMatches_MuSegIdx"};

  TTreeReaderArray<Char_t> seg_wheel{reader, "dtSegment_wheel"};
  TTreeReaderArray<Char_t> seg_station{reader, "dtSegment_station"};
  TTreeReaderArray<Char_t> seg_sector{reader, "dtSegment_sector"};
  TTreeReaderArray<Float_t> seg_time{reader, "dtSegment_seg2D_phi_t0"};
  TTreeReaderArray<Char_t> seg_nHitsPhi{reader, "dtSegment_seg2D_phi_nHits"};


  // Loop over all entries of the TTree or TChain using the TTreeReader
  while (reader.Next()) {
    for (int i_mu = 0; i_mu < *n_mu; ++i_mu) {
      // Preliminary cut, select tight muons in barrel
      if (!(mu_isTight[i_mu] && std::abs(mu_eta[i_mu]) < MAX_ABS_ETA && mu_pt[i_mu] > MIN_PT)) {
        continue;
      }

      // Fill muon control plots

      histos["mu_pt"]->Fill(mu_pt[i_mu]);
      histos["mu_n_staMatches"]->Fill(muon_staMatches_end[i_mu] - muon_staMatches_begin[i_mu]);

      histos2D["mu_eta_phi_tightmuons"]->Fill(mu_eta[i_mu],mu_phi[i_mu]);

      ////// define here mu_eta_phi plot for tight muons with a criteria selecting mu_matchedstations >= 3 ////

      std::size_t i_match = muon_staMatches_begin[i_mu];
      for (; i_match != muon_staMatches_end[i_mu]; ++i_match) {
        const std::size_t i_seg{mu_staMatches_MuSegIdx[i_match]};

        histos["dt_seg_wheel"]->Fill(seg_wheel[i_seg]);
        histos["dt_seg_station"]->Fill(seg_station[i_seg]);
        histos["dt_seg_sector"]->Fill(seg_sector[i_seg]);
        histos["dt_seg_time"]->Fill(seg_time[i_seg]);
   
       ///// fill again the segment time plots not as a part of "histos" //////            

        histos2D["dt_seg_WheelvsStation"]->Fill(seg_sector[i_seg],seg_wheel[i_seg]);

        ///// fill the plot here for hit multiplity using variable using "seg_nHitsPhi" for a matched segment with good quality reco muon/////


      } //// loop on standalone muons matched with DT segment


    }
  }

  /// canvas with log scale on y-axis /////
  TCanvas * canvasTMP = new TCanvas("canvasTMP","canvas",800,800);
   canvasTMP->SetGrid();
   canvasTMP->SetLogy();

 //// Canvas with normal axis ////
  TCanvas * canvasTMP1 = new TCanvas("canvasTMP1","canvas",800,800);
   canvasTMP1->SetGrid();

   TCanvas * canvasTMP2 = new TCanvas("canvasTMP2","canvas",800,800);
   canvasTMP2->SetGrid();

  std::filesystem::create_directory(RESULTS_FOLDER);

  auto dt_color = TColor::GetColorTransparent(kOrange - 2, 0.5);

  for (const auto &[name, histo] : histos) {
    TCanvas c{name.c_str(), name.c_str(), 600, 600};
    c.SetGrid();

    histo->SetFillColor(dt_color);
    histo->Draw();
    histo->SetMinimum(0.0);

    c.SaveAs(Form("%s/%s.png", RESULTS_FOLDER.c_str(), name.c_str()));
  }


  for (const auto &[name, histo2D] : histos2D) {
    TCanvas c1{name.c_str(), name.c_str(), 600, 600};
    c1.SetGrid();

    histo2D->Draw("colz");
    //2Dhisto->SetMinimum(0.0);

    c1.SaveAs(Form("%s/%s.png", RESULTS_FOLDER.c_str(), name.c_str()));
   }
}
